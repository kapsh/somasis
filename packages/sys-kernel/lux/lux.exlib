# Copyright 2014-2016 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
#
# Based in part on './packages/sys-kernel/linux-headers/linux-headers.exlib'
# which can be found in ::arbor's git repository.
#   Copyright 2007 Bryan Østergaard
#   Copyright 2010 Sterling X. Winter <replica@exherbo.org>
#   Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
#   Distributed under the terms of the GNU General Public License v2
#

require github [ user='somasis' release="v${PV}" suffix=tar.xz ]

export_exlib_phases src_install

SUMMARY="A Linux kernel updater for smarties"
SLOT="0"
LICENCES="ISC"

DEPENDENCIES="
    build+run:
        sys-devel/make
    run:
        app-shells/bash
        dev-scm/git
        sys-libs/ncurses
        virtual/sed
"

if ever is_scm;then
    DEPENDENCIES+="
        build:
            sys-apps/ronn
    "
fi

DEFAULT_SRC_COMPILE_PARAMS=(
    prefix=/usr
    exec_prefix=/usr/$(exhost --target)
    localstatedir=/var
    runstatedir=/run
    docdir=/usr/share/doc/${PNVR}
    sysconfdir=/etc
)

DEFAULT_SRC_INSTALL_PARAMS=(
    prefix=/usr
    exec_prefix=/usr/$(exhost --target)
    localstatedir=/var
    runstatedir=/run
    docdir=/usr/share/doc/${PNVR}
    sysconfdir=/etc
)

lux_src_install() {
    default

    # kernel uses different names for architectures than we do
    local _host_architectures=(
        arm-exherbo-linux-gnueabi:arm
        armv7-unknown-linux-gnueabi:arm
        armv7-unknown-linux-gnueabihf:arm
        i686-pc-linux-gnu:i386
        i686-pc-linux-musl:i386
        x86_64-pc-linux-gnu:x86_64
        x86_64-pc-linux-musl:x86_64
    )
    for entry in "${_host_architectures[@]}" ; do
        [[ ${entry%:*} == $(exhost --target) ]] || continue
        ARCH=${entry#*:}
        break
    done

    # since exherbo uses different paths, we should change what lux passes to make.
    # not sure how to handle this in the actual program yet, and exherbo's the only
    # distro that needs this, so this should do for now.
    MAKEOPTS="\$MAKEOPTS ARCH=${ARCH} HOSTCC=${CC} CROSS_COMPILE=$(exhost --tool-prefix)"
    edo cat >> "${IMAGE}"/etc/lux.conf <<EOF
## Exherbo-specific configuration
MAKEOPTS="$MAKEOPTS"

kernel_configure() {
    ln -s /usr/host/bin/$(exhost --tool-prefix)pkg-config pkg-config
    PATH="\${PWD}:\${PATH}" default_kernel_configure
}
EOF

}
