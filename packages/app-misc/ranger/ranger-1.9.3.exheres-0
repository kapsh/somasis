# Copyright 2017 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ tag="v${PV}" ]
require setup-py [ import=distutils test=pytest multibuild=false ]

SUMMARY="Console file manager with Vi-style key bindings"
SLOT="0"
LICENCES="GPL-3"

MYOPTIONS=""

PLATFORMS="~amd64"

DEPENDENCIES="
    recommendation:
        dev-python/chardet[python_abis:*(-)?] [[ description = [ ${PN} uses it for encoding detection; but doesn't require it ] ]]
    suggestion:
        app-admin/sudo [[ description = [ ${PN} can use sudo to run things as root ] ]]
        app-arch/libarchive [[ description = [ ${PN} can use libarchive's bsdtar for archive previewing ] ]]
        app-text/odt2txt [[ description = [ ${PN} can use odt2txt to view OpenDocument files ] ]]
        app-text/poppler [[ description = [ ${PN} can use poppler's pdf2text to preview PDF files ] ]]
        dev-python/Pygments [[ description = [ ${PN} can use Pygments' pygmentize for syntax highlighting ] ]]
        media/mediainfo [[ description = [ ${PN} can use mediainfo to view info about media files, surprisingly ] ]]
        media-libs/ExifTool [[ description = [ ${PN} can use exiftool to view EXIF data ] ]]
        media-libs/libcaca [[ description = [ ${PN} can use libcaca's img2text for ASCII-art image previews ] ]]
        net-p2p/transmission [[ description = [ ${PN} can use transmission's transmission-show to view .torrent information ] ]]
        net-www/w3m[imlib2] [[ description = [ ${PN} can use w3mimgdisplay to preview images ] ]]
        net-www/elinks [[ description = [ ${PN} can use elinks to preview HTML files ] ]]
        net-www/lynx [[ description = [ ${PN} can use lynx to preview HTML files ] ]]
"

src_prepare() {
    setup-py_src_prepare
    edo sed "s|share/doc/ranger|share/doc/${PNVR}|g" \
            -i setup.py ranger/config/*.conf ranger/core/*.py doc/*.1 doc/*.pod examples/*.conf
}

