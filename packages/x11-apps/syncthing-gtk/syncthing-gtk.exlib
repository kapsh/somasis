# Copyright 2014-2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require setup-py [ import=setuptools blacklist="3" multibuild=false ]
require github [ user='syncthing' tag="v${PV}" ]
require gtk-icon-cache

export_exlib_phases src_compile

SUMMARY="GTK3 based GUI and notification area icon for Syncthing"

SLOT="0"
LICENCES="GPL-2"

UPSTREAM_CHANGELOG="${HOMEPAGE}/releases"
UPSTREAM_DOCUMENTATION="
    ${HOMEPAGE}/blob/master/README.md [[ lang = en description = [ Readme ] ]]
"

DEPENDENCIES="
    build+run:
        !x11-apps/syncthing-gui [[
            description = [ You need to remove syncthing-gui and replace it with this, they changed names upstream ]
            resolution = uninstall-blocked-after
        ]]
    run:
        dev-python/python-dateutil[python_abis:2.7]
        gnome-bindings/pygobject:3[python_abis:2.7][cairo]
        gnome-desktop/librsvg:2[gobject-introspection]
        net-p2p/syncthing[>=0.13.0]
        x11-libs/gtk+:3[>=3.0][gobject-introspection]
    suggestion:
        cinnamon-desktop/nemo [[
            description = [ Integration with file managers, including emblems and context menu items ]
        ]]
        gnome-desktop/nautilus[gobject-introspection] [[
            description = [ Integration with file managers, including emblems and context menu items ]
        ]]
        dev-python/pyinotify [[
            description = [ Allow synchronization of file changes instantenously, rather than the polling used by default ]
        ]]
        x11-libs/libnotify[gobject-introspection] [[
            description = [ Show desktop notifications when synchronization state changes ]
        ]]
"

# there are no tests, and setup.py fails with an invalid command error
RESTRICT="test"

SETUP_PY_SRC_INSTALL_PARAMS=(
    --skip-build
)

syncthing-gtk_src_compile() {
    edo ${PYTHON} -B setup.py build_py --nostdownloader
    edo ${PYTHON} -B setup.py build_scripts
}
